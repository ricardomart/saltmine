#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response
from random import randint, shuffle

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)


USERNAME = "rmm2222"
PASSWORD = "1811"
DATABASEURI = "postgresql://%s:%s@w4111a.eastus.cloudapp.azure.com/proj1part2" \
    % (USERNAME, PASSWORD)


engine = create_engine(DATABASEURI)


@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request 
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request.

  The variable g is globally accessible.
  """
  try:
    g.conn = engine.connect()
  except:
    print "uh oh, problem connecting to database"
    import traceback; traceback.print_exc()
    g.conn = None

@app.teardown_request
def teardown_request(exception):
  """
  At the end of the web request, this makes sure to close the database connection.
  If you don't, the database could run out of memory!
  """
  try:
    g.conn.close()
  except Exception as e:
    pass


@app.route('/')
def index():
  """
  request is a special object that Flask provides to access web request information:

  request.method:   "GET" or "POST"
  request.form:     if the browser submitted a form, this contains the data in the form
  request.args:     dictionary of URL arguments, e.g., {a:1, b:2} for http://localhost?a=1&b=2

  See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
  """

  # DEBUG: this is debugging code to see what request looks like
  # print request.args

  # provide different prompts on the database homepage
  prompt = randint(1,3)
  prompt1 = True if prompt == 1 else False
  prompt2 = True if prompt == 2 else False
  prompt3 = True if prompt == 3 else False

  if prompt == 1:

    cursor = g.conn.execute("SELECT * FROM champ_skins where price > 975 order by champ_name")

    skins = []
    for result in cursor:
      skins.append(result)

    if len(skins) > 5:
      shuffle(skins)
      skins = skins[0:5]

    cursor.close()
    context = dict(data = skins)

  elif prompt == 2:

    cursor = g.conn.execute("select champ_name, faction from champions order by champ_name")

    factions = []
    for result in cursor:
      factions.append(result)

    if len(factions) > 5:
      shuffle(factions)
      factions = factions[0:5]

    cursor.close()
    context = dict(data = factions)

  elif prompt == 3:

    cursor = g.conn.execute("select * from compatible_with order by champ_name1")

    champ_pairs = []
    for result in cursor:
      champ_pairs.append(result)

    # select 10 random items our query
    if len(champ_pairs) > 5:
      shuffle(champ_pairs)
      champ_pairs = champ_pairs[0:5]

    cursor.close()
    context = dict(data = champ_pairs)


  return render_template("index.html", prompt1=prompt1, prompt2=prompt2, prompt3=prompt3, **context)


@app.route('/skins')
def skins():

  cursor = g.conn.execute("SELECT * FROM champ_skins order by champ_name, price")
  skins = []
  for result in cursor:
    skins.append(result)

  cursor.close()
  context = dict(data = skins)

  return render_template("skins.html", **context)


@app.route('/all_champions')
def all_champions():

  cursor = g.conn.execute("select champ_name from champions order by champ_name")
  champs = []
  for result in cursor:
    champs.append(result[0])

  cursor.close()
  context = dict(champs = champs)

  return render_template("all_champions.html", **context)


@app.route('/champions', methods=['GET', 'POST'])
def champions():

  if request.method == 'GET':
    return redirect('/')
    
  pname = request.form['name']
  name = pname.replace(';','');
  cursor = g.conn.execute("select * from champions where champ_name = '%s'" % name)

  champ_info = cursor.fetchone()

  found = True
  if champ_info == None:
    found = False

  abilities = []
  skins = []
  compatible = []
  strong = []

  if found:

    cursor = g.conn.execute("select * from champ_abilities where champ_name = '%s'" % name)

    for result in cursor:
      abilities.append(result)

    cursor = g.conn.execute("select skin_name, price from champ_skins where champ_name = '%s'" % name)

    for result in cursor:
      skins.append(result)

    cursor = g.conn.execute("select champ_name2 from compatible_with where champ_name1 = '%s'" % name)

    for result in cursor:
      compatible.append(result)

    cursor = g.conn.execute("select champ_name2 from strong_against where champ_name1 = '%s'" % name)

    for result in cursor:
      strong.append(result)

  cursor.close()
  context = dict(name = name, found = found, abilities = abilities, skins = skins, compatible = compatible, strong = strong)

  return render_template("champions.html", champ_info=champ_info, **context)


@app.route('/types', methods=['GET', 'POST'])
def types():

  if request.method == 'GET':
    return redirect('/')

  pname = request.form['name']
  name = pname.replace(';','')
  cursor = g.conn.execute("select champ_name from is_type where type_name = '%s'" % name)

  champs = []
  for result in cursor:
    champs.append(result[0])

  type_list = []
  description = ''

  # determine at this point if we have a valid or invalid entry
  # if invalid, show the possible entries

  found = False if len(champs) == 0 else True
  if not found:
    cursor = g.conn.execute("select type_name from types")
    for result in cursor:
      type_list.append(result[0]) # adds 'mage', 'tank', etc.

  else:
    cursor = g.conn.execute("select description from types where type_name = '%s'" % name)
    description = cursor.fetchone()[0]

  cursor.close()
  context = dict(data = champs) if found else dict(data = type_list)

  return render_template("types.html", name=name, found=found, description=description, **context)


@app.route('/filter', methods=['GET', 'POST'])
def filter():

  if request.method == 'GET':
    return redirect('/')

  achamp_type = request.form['type']
  champ_type = achamp_type.replace(';','')
  achamp_role = request.form['role']
  champ_role = achamp_role.replace(';','')
  achamp_compatible = request.form['compatible']
  champ_compatible = achamp_compatible.replace(';','') 
  achamp_counter = request.form['counter']
  champ_counter = achamp_counter.replace(';','')

  psqlquery = ""
  psqlquery += "select distinct champions.champ_name, champions.nickname, champions.faction, champions.price from champions inner join is_type on (champions.champ_name = is_type.champ_name) inner join plays on (plays.champ_name = champions.champ_name) inner join compatible_with on (champions.champ_name = compatible_with.champ_name1) inner join strong_against on (champions.champ_name = strong_against.champ_name1) where "
  # queries champ_name, nickname, faction, price

  some_filter = False

  if champ_type != "":
    psqlquery += "is_type.type_name = '%s' and " % champ_type
    some_filter = True

  if champ_role != "":
    psqlquery += "plays.role_type = '%s' and " % champ_role
    some_filter = True

  if champ_compatible != "":
    psqlquery += "compatible_with.champ_name2 = '%s' and " % champ_compatible
    some_filter = True

  if champ_counter != "":
    psqlquery += "strong_against.champ_name2 = '%s' and " % champ_counter
    some_filter = True

  if some_filter:
    psqlquery = psqlquery[:-4]
  else:
    psqlquery = psqlquery[:-6]

  # run request
  cursor = g.conn.execute("%s" % psqlquery)

  champs = []
  for result in cursor:
    champs.append(result)

  found = True
  if len(champs) == 0:
    found = False

  cursor.close()
  context = dict(champs = champs)

  return render_template("filter.html", type = champ_type, role = champ_role, compatible = champ_compatible, counter = champ_counter, found = found, **context)


@app.route('/vote', methods=['GET', 'POST'])
def vote():

  has_voted = True
  champs = []

  if request.method == 'GET':
    has_voted = False

    cursor = g.conn.execute('select champ_name from champions order by champ_name')

    for result in cursor:
      champs.append(result[0])

  # else, they voted and we have to update database
  else:
    champ_name = request.form['name']
    cursor = g.conn.execute("update champions set count = count + 1 where champ_name = '%s'" % champ_name)

  cursor.close()
  context = dict(champs = champs)

  return render_template('vote.html', has_voted = has_voted, **context)


@app.route('/results')
def results():

  total_votes = 0

  cursor = g.conn.execute('select champ_name, count from champions order by champ_name')

  champs = []
  for result in cursor:
    champs.append(result)
    total_votes += result[1]

  cursor.close()
  context = dict(champs = champs)

  # cover divide by 0
  if total_votes == 0:
    total_votes = 1

  return render_template('results.html', total_votes = total_votes, **context)


@app.route('/login')
def login():
  abort(401)
  this_is_never_executed()


if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using:

        python server.py

    Show the help text using:

        python server.py --help

    """

    HOST, PORT = host, port
    print "running on %s:%d" % (HOST, PORT)
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
